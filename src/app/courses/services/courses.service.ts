import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

import { Course } from '../model/course';
import { delay, first } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  private readonly API = 'https://crud-spring.fly.dev/api/courses'
  //private readonly API = 'api/courses'
  //private readonly API = '/assets/courses.json'

  constructor(private httpClient: HttpClient) { }

  list(){
    return this.httpClient.get<Course[]>(this.API)
      .pipe(
        first()
      );
  }

  saveCourse(course: Partial<Course>){
    return this.httpClient.post<Course>(this.API, course);
  }

  findById(id: string){
    return this.httpClient.get<Course>(`${this.API}/${id}`);
  }
}
