import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NonNullableFormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

import { CoursesService } from '../../services/courses.service';

@Component({
  selector: 'app-course-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.scss']
})
export class CourseFormComponent implements OnInit {

  form = this.formBuilder.group({
    name: [''],
    category: ['']
  });

  constructor(
    private coursesService: CoursesService,
    private formBuilder: NonNullableFormBuilder,
    private location: Location,
    private _snackBar: MatSnackBar
  ) {

  }

  onSubmit() {
    this.coursesService.saveCourse(this.form.value)
      .subscribe({
        next: (r) => {this.onSuccess()},
        error: (e) => {this.onError()}
      });
  }

  onCancel() {
    this.location.back();
  }

  private onError(){
    this._snackBar.open('Ocorreu um erro', '', {duration: 5000});
  }

  private onSuccess(){
    this._snackBar.open('Deu certinho', '', {duration: 5000})
    this.onCancel();
  }

  ngOnInit(): void {
  }

}
