import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'category'
})
export class CategoryPipe implements PipeTransform {

  transform(value: string): string {

    switch(value) {
      case 'Family' : return 'house';
      case 'Social Midia' : return 'cloudy';
      case 'Job' : return 'work';
      case 'Support' : return 'support agent';
    }

    return 'code';

  }

}
