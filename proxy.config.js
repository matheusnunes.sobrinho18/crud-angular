const PROXY_CONFIG = [
  {
    context: ['/api'],
    target: 'https://crud-spring.fly.dev/',
    secure: true,
    logLevel: 'debug'
  }
];

module.exports = PROXY_CONFIG;
